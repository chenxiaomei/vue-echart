import Vue from 'vue'
import Router from 'vue-router'
import EchartsLine from '@/components/EchartsLine'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'EchartsLine',
      component: EchartsLine
    },
    {
      path: '/RegionalLineChart',
      name: 'RegionalLineChart',
      component: r => require(['@/components/RegionalLineChart'], r),
      meta: {
        title: '区域折线图'
      }
    }
  ]
})
